﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryRepository.Controller
{
    public interface IUserRepository<TEntity>
    where TEntity : class     
    {
        Task<TEntity> GetByIdAsync(int id);
        IQueryable<TEntity> SearchFor(Expression<Func<TEntity, bool>> predicate);

        IQueryable<TEntity> GetAll();

        Task EditAsync(TEntity entity);

        Task InsertAsync(TEntity entity);

        Task DeleteAsync(TEntity entity);
        Task SaveChangesAsync(TEntity entity);
        Task<TEntity> FindAsync(int id);
    }
}
