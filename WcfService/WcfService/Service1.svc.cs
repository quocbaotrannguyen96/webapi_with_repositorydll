﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using ClassLibraryRepository.Controller;


namespace WcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        
        private UserRepository<user> userRepo;
        private testCRUDEntities db = new testCRUDEntities();
        public Service1()
        {
            testCRUDEntities db = new testCRUDEntities();
            userRepo = new UserRepository<user>(db);
        }

        public Task DeleteAsync(user entity)
        {
            return userRepo.DeleteAsync(entity);
        }
        public async Task Deleteuser(int id)
        {

            user usr = await FindAsync(id);
            

             await userRepo.DeleteAsync(usr);
           
            
        }

        public Task EditAsync(user entity)
        {
            return userRepo.EditAsync(entity);
        }

        public Task<user> FindAsync(int id)
        {
            return userRepo.FindAsync(id);
        }

        public IQueryable<user> GetAll()
        {
            return userRepo.GetAll();
        }

        public Task InsertAsync(user entity)
        {
            return userRepo.InsertAsync(entity);
        }
        
    }
}
